package com.bods.eldrain.hcitestapp.models

import com.google.gson.annotations.SerializedName

data class ArticleData(@SerializedName("data") val data:List<ArticleResponse>) {
}