package com.bods.eldrain.hcitestapp.models

import com.google.gson.annotations.SerializedName

data class ArticleResponse(
    @SerializedName("section") val section:String,
    @SerializedName("items") val articleItem:List<ArticleModel>,
    @SerializedName("section_title") val sectionTitle:String) {
}