package com.bods.eldrain.hcitestapp.models

import com.google.gson.annotations.SerializedName

data class ProductResponse(
    @SerializedName("section") val section:String,
    @SerializedName("items") val productItem:List<ProductModel>) {
}