package com.bods.eldrain.hcitestapp.models

import com.google.gson.annotations.SerializedName

data class ProductData(@SerializedName("data") val data:List<ProductResponse>) {
}