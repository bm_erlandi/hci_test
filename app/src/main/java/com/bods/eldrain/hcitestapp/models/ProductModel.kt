package com.bods.eldrain.hcitestapp.models

import com.google.gson.annotations.SerializedName

data class ProductModel (
    @SerializedName("product_name") val productName:String,
    @SerializedName("product_image") val productImg:String,
    @SerializedName("link") val productLink:String){
}