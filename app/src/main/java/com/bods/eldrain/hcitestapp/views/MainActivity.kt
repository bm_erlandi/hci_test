package com.bods.eldrain.hcitestapp.views

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.*
import com.bods.eldrain.hcitestapp.R
import com.bods.eldrain.hcitestapp.models.ArticleModel
import com.bods.eldrain.hcitestapp.models.ArticleData
import com.bods.eldrain.hcitestapp.models.ProductData
import com.bods.eldrain.hcitestapp.models.ProductModel
import com.bods.eldrain.hcitestapp.nets.HciAPI
import com.bods.eldrain.hcitestapp.nets.HciServices
import com.bods.eldrain.hcitestapp.views.adapter.ArticleAdapter
import com.bods.eldrain.hcitestapp.views.adapter.ProductAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    private lateinit var articleLayout: RecyclerView
    private lateinit var articleModel: List<ArticleModel>
    private lateinit var articleSecTitle: TextView
    private lateinit var productLayout: RecyclerView
    private lateinit var productModel: List<ProductModel>

    private lateinit var articleAdapter: ArticleAdapter
    private lateinit var productAdapter: ProductAdapter

    private var x: Int = 0
    private var y: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val hciAPI : HciAPI = HciServices.create()
        getArticles(hciAPI)
        getProducts(hciAPI)
    }

    private fun getProducts(hciAPI: HciAPI) {
        productLayout = findViewById(R.id.productHolderGridView)
        productLayout.layoutManager = GridLayoutManager(applicationContext,3)
        productLayout.setHasFixedSize(true)

        val call : Call<ProductData> = hciAPI.getProducts()
        call.enqueue(object : Callback<ProductData>{
            override fun onFailure(call: Call<ProductData>, t: Throwable) {
                Toast.makeText(applicationContext,"FAILED : "+t.message,Toast.LENGTH_LONG).show()
                Log.d("MSG",t.message)
            }

            override fun onResponse(call: Call<ProductData>, response: Response<ProductData>) {
                if(response.isSuccessful){
                    val responseJson = response.body()!!
                    for(i in responseJson.data){
                        val sectionName = i.section
                        if(sectionName == "products"){
                            productModel = i.productItem
                            productAdapter = ProductAdapter(productModel){
                                itemClicked(i.productItem[responseJson.data.indexOf(i)].productLink)
                            }
                            productLayout.adapter = productAdapter
                        }
                    }

                    x=1
                    menuOnLoad(x,y)
                }
            }

        })
    }

    private fun getArticles(hciAPI: HciAPI){
        articleSecTitle = findViewById(R.id.sectionTitleTxt)

        articleLayout = findViewById(R.id.articleHolderRecyclerView)
        articleLayout.layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false )
        articleLayout.setHasFixedSize(true)

        val call : Call<ArticleData> = hciAPI.getArticles()
        call.enqueue(object : Callback<ArticleData>{
            override fun onFailure(call: Call<ArticleData>, t: Throwable) {
                Toast.makeText(applicationContext,"FAILED : "+t.message,Toast.LENGTH_LONG).show()
                Log.d("MSG",t.message)
            }

            override fun onResponse(call: Call<ArticleData>, response: Response<ArticleData>) {
                if(response.isSuccessful){
                    val responseJson = response.body()!!
                    for (i in responseJson.data){
                        val sectionName = i.section
                        if(sectionName == "articles"){
                            articleSecTitle.text = i.sectionTitle
                            articleModel = i.articleItem
                            articleAdapter = ArticleAdapter(articleModel) {
                                itemClicked(i.articleItem[responseJson.data.indexOf(i)].articleLink)
                            }
                            articleLayout.adapter = articleAdapter
                        }
                    }

                    y=1
                    menuOnLoad(x,y)
                }
            }

        })
    }

    private fun itemClicked(link: String){
        startActivity<WebViewActivity>("link" to link)
    }

    private fun menuOnLoad(x: Int, y: Int){
        if(x==1 && y==1){
            menuOnLoadScreen.visibility = View.GONE
            mainScrollView.visibility = View.VISIBLE
            mainScrollView.isSmoothScrollingEnabled = true
        }
    }
}
