package com.bods.eldrain.hcitestapp.nets

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object HciServices {

    private const val url = "https://private-a8e48-hcidtest.apiary-mock.com/"

    fun create():HciAPI{

        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(url)
            .build()

        return retrofit.create(HciAPI::class.java)
    }
}