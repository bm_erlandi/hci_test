package com.bods.eldrain.hcitestapp.views.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bods.eldrain.hcitestapp.R
import com.bods.eldrain.hcitestapp.models.ProductModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.layout_adapter_product_section.view.*

class ProductAdapter(private val productItems:List<ProductModel>, val clickListener: (ProductModel) -> Unit):
    RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.layout_adapter_product_section, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.bind(productItems[pos],clickListener)
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private var view: View = itemView
        private var eachProduct: ProductModel? = null

        fun bind(eachProduct: ProductModel, clickListener: (ProductModel) -> Unit){
            this.eachProduct = eachProduct
            val productImgUrl = eachProduct.productImg

            view.productTitleTxt.text = eachProduct.productName
            Glide.with(view.context).load(productImgUrl).into(view.productImg!!)

            view.setOnClickListener { clickListener(eachProduct) }
        }
    }

    override fun getItemCount(): Int = productItems.size

}
