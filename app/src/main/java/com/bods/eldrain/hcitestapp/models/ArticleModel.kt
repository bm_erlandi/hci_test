package com.bods.eldrain.hcitestapp.models

import com.google.gson.annotations.SerializedName

data class ArticleModel(
    @SerializedName("article_title") val articleTitle:String,
    @SerializedName("article_image") val articleImg:String,
    @SerializedName("link") val articleLink:String) {
}