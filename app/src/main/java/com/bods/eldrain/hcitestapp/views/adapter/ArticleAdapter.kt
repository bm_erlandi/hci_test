package com.bods.eldrain.hcitestapp.views.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bods.eldrain.hcitestapp.R
import com.bods.eldrain.hcitestapp.models.ArticleModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.layout_adapter_article_section.view.*

class ArticleAdapter(private val articleItems: List<ArticleModel>, val clickListener:(ArticleModel) -> Unit):
    RecyclerView.Adapter<ArticleAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.layout_adapter_article_section, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.bind(articleItems[pos], clickListener)
    }

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        private var view : View = itemView
        private var eachArticle : ArticleModel? = null

        fun bind(eachArticle: ArticleModel, clickListener: (ArticleModel) -> Unit){
            this.eachArticle = eachArticle
            val articleImgUrl = eachArticle.articleImg

            view.articleTxt.text = eachArticle.articleTitle
            Glide.with(view.context).load(articleImgUrl.toString()).into(view.articleImg)

            view.setOnClickListener { clickListener(eachArticle) }
        }

    }

    override fun getItemCount()= articleItems.size
}