package com.bods.eldrain.hcitestapp.nets

import com.bods.eldrain.hcitestapp.models.ArticleData
import com.bods.eldrain.hcitestapp.models.ProductData
import com.bods.eldrain.hcitestapp.models.ProductModel
import retrofit2.Call
import retrofit2.http.GET

interface HciAPI {

    @GET("home")
    fun getProducts():Call<ProductData>

    @GET("home")
    fun getArticles():Call<ArticleData>
}